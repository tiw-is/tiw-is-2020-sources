package fr.univlyon1.tiw1.gestionvm.models;

/**
 * Serialization-ready representation of a virtual machine.
 */
public class VMDTO {
    private Long id;
    private String ip;
    private String os;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public VM asVM() {
        VM vm = new VM();
        vm.setId(id);
        vm.setIp(ip);
        vm.setOs(os);
        return vm;
    }
}
