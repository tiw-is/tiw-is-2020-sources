package fr.univlyon1.tiw1.gestionrdp;

import fr.univlyon1.tiw1.gestionrdp.exception.*;
import fr.univlyon1.tiw1.gestionrdp.services.GestionRDP;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class ClientTests {

    private GestionRDP gestionRDP;

    @BeforeEach
    public void setup() {
        // FIXME: instancier correctement GestionRDP
        // gestionRDP = new GestionRDP();
    }

    @AfterEach
    public void teardown() {
        gestionRDP = null;
    }

    @Test
    void testSetupIsOK() {
        // Empty as the serveur is built in setup
        assertNotNull(gestionRDP);
    }

    @Test
    void client1() throws SQLException, ConnexionQuotaExceededException, UnknownVMException, UnknownSessionException, UnknownConnexionException {
        String session = gestionRDP.demarreSession("1.1.1.1");
        gestionRDP.stopSession(session);
        assertNotNull(gestionRDP.fournisseurInfos());
    }

    @Test
    void quotaTests() throws SQLException, ConnexionQuotaExceededException, UnknownVMException, UnknownSessionException, UnknownConnexionException {
        String user = "toto";
        Stack<String> s = new Stack<>();
        s.push(gestionRDP.demarreSession("1.1.1.1"));
        var firstFournisseurOpt = gestionRDP.fournisseurInfos().stream().findFirst();
        assertTrue(firstFournisseurOpt.isPresent());
        int max = firstFournisseurOpt.get().getQuota();
        for (int i = 1; i < max; i++) {
            s.push(gestionRDP.demarreSession("1.1.1.1"));
        }
        assertThrows(ConnexionQuotaExceededException.class, () -> gestionRDP.demarreSession("1.1.1.1"));
        while (!s.isEmpty()) {
            gestionRDP.stopSession(s.pop());
        }
    }
}