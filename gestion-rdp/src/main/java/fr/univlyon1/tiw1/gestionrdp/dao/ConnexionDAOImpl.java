package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.models.Connexion;
import fr.univlyon1.tiw1.gestionrdp.services.DBAccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnexionDAOImpl implements ConnexionDAO {
    private DBAccess dbAccess;
    private FournisseurDAO fournisseurDAO;
    private PreparedStatement insertStatement;
    private PreparedStatement findByIdStatement;
    private PreparedStatement deleteByIdStatement;

    public ConnexionDAOImpl(FournisseurDAO fournisseurDAO, DBAccess dbAccess)
            throws SQLException {
        this.fournisseurDAO = fournisseurDAO;
        this.dbAccess = dbAccess;
        createTable();
        initStatements();
    }

    private void createTable() throws SQLException {
        try (PreparedStatement stat = dbAccess.getConnection().prepareStatement(
                "CREATE TABLE IF NOT EXISTS connexion(" +
                        "id SERIAL primary key," +
                        "fournisseur varchar(255))")) {
            stat.execute();
        }
    }

    private void initStatements() throws SQLException {
        insertStatement = dbAccess.getConnection().prepareStatement(
                "INSERT INTO connexion(fournisseur) VALUES (?) RETURNING id");
        findByIdStatement = dbAccess.getConnection().prepareStatement(
                "SELECT fournisseur FROM connexion WHERE id = ?");
        deleteByIdStatement = dbAccess.getConnection().prepareStatement(
                "DELETE FROM connexion WHERE id = ?");
    }

    @Override
    public void save(Connexion connexion) throws SQLException {
        insertStatement.setString(1, connexion.getFournisseur().getName());
        ResultSet rs = insertStatement.executeQuery();
        if (rs.next()) {
            connexion.setId(rs.getInt(1));
        } else {
            throw new SQLException("Failed to create connexion");
        }
    }

    @Override
    public Connexion getById(int connexionId) throws SQLException, UnknownConnexionException {
        findByIdStatement.setInt(1, connexionId);
        ResultSet rs = findByIdStatement.executeQuery();
        if (rs.next()) {
            return new Connexion(
                    fournisseurDAO.getFournisseur(rs.getString(1)),
                    connexionId);
        } else {
            throw new UnknownConnexionException(String.valueOf(connexionId));
        }
    }

    @Override
    public void deleteById(int connexionId) throws SQLException, UnknownConnexionException {
        deleteByIdStatement.setInt(1, connexionId);
        int count = deleteByIdStatement.executeUpdate();
        if (count == 0) {
            throw new UnknownConnexionException();
        }
    }

}
