package fr.univlyon1.tiw1.gestionrdp.services;

import fr.univlyon1.tiw1.gestionrdp.dao.ConnexionDAO;
import fr.univlyon1.tiw1.gestionrdp.exception.ConnexionQuotaExceededException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.models.Connexion;
import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.Fournisseur;

import java.sql.SQLException;

public class ConnexionService {

    private ConnexionDAO connexionDAO;

    public ConnexionService(ConnexionDAO connexionDAO) {
        this.connexionDAO = connexionDAO;
    }

    public Connexion openConnexion(Fournisseur fournisseur)
            throws ConnexionQuotaExceededException, SQLException {
        if (fournisseur.getConsumed() >= fournisseur.getQuota()) {
            throw new ConnexionQuotaExceededException();
        }
        Connexion connexion = new Connexion();
        connexion.setFournisseur(fournisseur);
        connexionDAO.save(connexion);
        fournisseur.setConsumed(fournisseur.getConsumed() + 1);
        return connexion;
    }

    public void releaseConnexion(int connexionId) throws SQLException, UnknownConnexionException {
        Connexion connexion = connexionDAO.getById(connexionId);
        Fournisseur f = connexion.getFournisseur();
        connexionDAO.deleteById(connexionId);
        f.setConsumed(f.getConsumed() - 1);
    }

}
