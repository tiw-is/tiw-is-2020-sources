package fr.univlyon1.tiw1.gestionrdp.services;

import fr.univlyon1.tiw1.gestionrdp.dao.FournisseurDAO;
import fr.univlyon1.tiw1.gestionrdp.dao.VMDAO;
import fr.univlyon1.tiw1.gestionrdp.exception.ConnexionQuotaExceededException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownSessionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownVMException;
import fr.univlyon1.tiw1.gestionrdp.models.Session;
import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.Fournisseur;
import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.VM;

import java.sql.SQLException;
import java.util.Collection;

public class GestionRDP {

    private final VMDAO vmDAO;
    private final SessionService sessionService;
    private final FournisseurDAO fournisseurDAO;

    public GestionRDP(VMDAO vmDAO,
                      SessionService sessionService,
                      FournisseurDAO fournisseurDAO) {
        this.vmDAO = vmDAO;
        this.sessionService = sessionService;
        this.fournisseurDAO = fournisseurDAO;
    }

    public String demarreSession(String ip)
            throws UnknownVMException, SQLException, ConnexionQuotaExceededException {
        VM vm = vmDAO.getVM(ip);
        Session session = sessionService.createSession(vm);
        return session.getUuid();
    }

    public void stopSession(String sessionId)
            throws SQLException, UnknownSessionException, UnknownConnexionException, UnknownVMException {
        sessionService.stopSession(sessionId);
    }

    public Collection<Fournisseur> fournisseurInfos() {
        return fournisseurDAO.getAllFournisseurs();
    }

}
