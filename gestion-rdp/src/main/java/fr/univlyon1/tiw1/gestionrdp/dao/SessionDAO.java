package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownSessionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownVMException;
import fr.univlyon1.tiw1.gestionrdp.models.Session;

import java.sql.SQLException;

public interface SessionDAO {
    void save(Session session) throws SQLException;

    Session getSession(String sessionId) throws SQLException, UnknownSessionException, UnknownConnexionException, UnknownVMException;

    void deleteById(String sessionUUID) throws SQLException, UnknownSessionException;
}
