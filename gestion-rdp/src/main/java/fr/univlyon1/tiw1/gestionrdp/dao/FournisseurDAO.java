package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.Fournisseur;

import java.util.Collection;

public interface FournisseurDAO {
    Fournisseur getFournisseur(String name);

    Collection<Fournisseur> getAllFournisseurs();
}
