package fr.univlyon1.tiw1.gestionrdp.models.gestionvm;

public class Fournisseur {
    private String name;
    private int quota;
    private int consumed;

    public Fournisseur() {
    }

    public Fournisseur(String name, int quota, int consumed) {
        this.name = name;
        this.quota = quota;
        this.consumed = consumed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public int getConsumed() {
        return consumed;
    }

    public void setConsumed(int consumed) {
        this.consumed = consumed;
    }
}
