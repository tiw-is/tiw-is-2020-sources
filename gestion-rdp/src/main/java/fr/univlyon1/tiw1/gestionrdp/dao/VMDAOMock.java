package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.exception.UnknownVMException;
import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.VM;

public class VMDAOMock implements VMDAO {
    private FournisseurDAO fournisseurDAO;

    public VMDAOMock(FournisseurDAO fournisseurDAO) {
        this.fournisseurDAO = fournisseurDAO;
    }

    @Override
    public VM getVM(String ip) throws UnknownVMException {
        if (!ip.endsWith("0")) {
            return new VM(ip, fournisseurDAO.getFournisseur("fournisseur1"));
        } else {
            throw new UnknownVMException(ip);
        }
    }
}
