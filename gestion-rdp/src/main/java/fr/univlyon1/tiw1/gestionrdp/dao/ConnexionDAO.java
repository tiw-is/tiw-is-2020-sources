package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.models.Connexion;

import java.sql.SQLException;

public interface ConnexionDAO {
    void save(Connexion connexion) throws SQLException;

    Connexion getById(int connexionId) throws SQLException, UnknownConnexionException;

    void deleteById(int connexionId) throws SQLException, UnknownConnexionException;
}
