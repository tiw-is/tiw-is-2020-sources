package fr.univlyon1.tiw1.gestionrdp.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBAccess implements AutoCloseable {
    private Connection connection = null;
    private String url = "jdbc:postgresql://localhost/gestionvm";
    private String user = "gestionvm";
    private String password = "gestionvmpwd";

    public DBAccess(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public DBAccess() {
    }

    public Connection getConnection() throws SQLException {
        if (connection == null) {
            Properties props = new Properties();
            props.setProperty("user", user);
            props.setProperty("password", password);
            connection = DriverManager.getConnection(url, props);
        }
        return connection;
    }

    @Override
    public void close() throws Exception {
        connection.close();
        connection = null;
    }
}
