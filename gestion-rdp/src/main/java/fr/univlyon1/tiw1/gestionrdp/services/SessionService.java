package fr.univlyon1.tiw1.gestionrdp.services;

import fr.univlyon1.tiw1.gestionrdp.dao.SessionDAO;
import fr.univlyon1.tiw1.gestionrdp.exception.ConnexionQuotaExceededException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownSessionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownVMException;
import fr.univlyon1.tiw1.gestionrdp.models.Connexion;
import fr.univlyon1.tiw1.gestionrdp.models.Session;
import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.VM;

import java.sql.SQLException;
import java.util.UUID;

public class SessionService {
    private SessionDAO sessionDAO;
    private ConnexionService connexionService;

    public SessionService(SessionDAO sessionDAO, ConnexionService connexionService) {
        this.sessionDAO = sessionDAO;
        this.connexionService = connexionService;
    }

    public Session createSession(VM vm) throws SQLException, ConnexionQuotaExceededException {
        String id = UUID.randomUUID().toString();
        Connexion connexion = connexionService.openConnexion(vm.getFournisseur());
        Session session = new Session(id, connexion, vm);
        sessionDAO.save(session);
        return session;
    }

    public void stopSession(String sessionId)
            throws SQLException, UnknownSessionException, UnknownConnexionException, UnknownVMException {
        Session session = sessionDAO.getSession(sessionId);
        Connexion connexion = session.getConnexion();
        sessionDAO.deleteById(session.getUuid());
        connexionService.releaseConnexion(connexion.getId());
    }
}
