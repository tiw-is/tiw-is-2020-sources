package fr.univlyon1.tiw1.gestionrdp.models;

import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.Fournisseur;

public class Connexion {
    private Fournisseur fournisseur;
    private int id;

    public Connexion() {
    }

    public Connexion(Fournisseur fournisseur, int id) {
        this.fournisseur = fournisseur;
        this.id = id;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
