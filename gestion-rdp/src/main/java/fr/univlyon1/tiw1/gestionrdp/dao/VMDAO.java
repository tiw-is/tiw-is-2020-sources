package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.exception.UnknownVMException;
import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.VM;

public interface VMDAO {
    VM getVM(String ip) throws UnknownVMException;
}
