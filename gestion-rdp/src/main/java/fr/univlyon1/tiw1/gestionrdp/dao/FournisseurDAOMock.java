package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.Fournisseur;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FournisseurDAOMock implements FournisseurDAO {

    private Map<String, Fournisseur> fournisseurs = new HashMap<>();

    @Override
    public Fournisseur getFournisseur(String name) {
        return fournisseurs.computeIfAbsent(name, n -> new Fournisseur(n, 5, 0));
    }

    @Override
    public Collection<Fournisseur> getAllFournisseurs() {
        return Collections.unmodifiableCollection(fournisseurs.values());
    }
}
