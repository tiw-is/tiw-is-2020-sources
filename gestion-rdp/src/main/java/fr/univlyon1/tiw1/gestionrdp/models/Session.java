package fr.univlyon1.tiw1.gestionrdp.models;

import fr.univlyon1.tiw1.gestionrdp.models.gestionvm.VM;

public class Session {
    private String uuid;
    private Connexion connexion;
    private VM vm;

    public Session(String uuid, Connexion connexion, VM vm) {
        this.uuid = uuid;
        this.connexion = connexion;
        this.vm = vm;
    }

    public Session() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Connexion getConnexion() {
        return connexion;
    }

    public void setConnexion(Connexion connexion) {
        this.connexion = connexion;
    }

    public VM getVm() {
        return vm;
    }

    public void setVm(VM vm) {
        this.vm = vm;
    }
}
