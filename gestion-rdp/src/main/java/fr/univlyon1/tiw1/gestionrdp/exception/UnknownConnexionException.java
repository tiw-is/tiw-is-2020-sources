package fr.univlyon1.tiw1.gestionrdp.exception;

public class UnknownConnexionException extends Exception {
    public UnknownConnexionException() {
    }

    public UnknownConnexionException(String message) {
        super(message);
    }

    public UnknownConnexionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownConnexionException(Throwable cause) {
        super(cause);
    }

    public UnknownConnexionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
