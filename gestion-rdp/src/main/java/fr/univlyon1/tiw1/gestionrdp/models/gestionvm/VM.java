package fr.univlyon1.tiw1.gestionrdp.models.gestionvm;

public class VM {
    private String ip;
    private Fournisseur fournisseur;

    public VM(String ip, Fournisseur fournisseur) {
        this.ip = ip;
        this.fournisseur = fournisseur;
    }

    public VM() {
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }
}
