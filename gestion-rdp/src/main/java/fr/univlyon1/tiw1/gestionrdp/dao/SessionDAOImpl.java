package fr.univlyon1.tiw1.gestionrdp.dao;

import fr.univlyon1.tiw1.gestionrdp.exception.UnknownConnexionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownSessionException;
import fr.univlyon1.tiw1.gestionrdp.exception.UnknownVMException;
import fr.univlyon1.tiw1.gestionrdp.models.Session;
import fr.univlyon1.tiw1.gestionrdp.services.DBAccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionDAOImpl implements SessionDAO {
    private final DBAccess dbAccess;
    private PreparedStatement insertStatement;
    private PreparedStatement findByIdStatement;
    private PreparedStatement deleteByIdStatement;
    private final ConnexionDAO connexionDAO;
    private final VMDAO vmService;

    public SessionDAOImpl(DBAccess dbAccess,
                          ConnexionDAO connexionDAO,
                          VMDAO vmService)
            throws SQLException {
        this.dbAccess = dbAccess;
        this.connexionDAO = connexionDAO;
        this.vmService = vmService;
        createSessionTable();
        initStatements();
    }

    private void createSessionTable() throws SQLException {
        try (PreparedStatement stat = dbAccess.getConnection().prepareStatement(
                "CREATE TABLE IF NOT EXISTS session(" +
                        "id varchar (36) primary key, " +
                        "username varchar(255), " +
                        "connexion integer, " +
                        "ip varchar(64))")) {
            stat.execute();
        }
    }

    private void initStatements() throws SQLException {
        insertStatement = dbAccess.getConnection().prepareStatement(
                "INSERT INTO session(id,connexion,ip) VALUES (?,?,?)");
        findByIdStatement = dbAccess.getConnection().prepareStatement(
                "SELECT id, connexion, ip FROM session WHERE id = ?");
        deleteByIdStatement = dbAccess.getConnection().prepareStatement(
                "DELETE FROM session WHERE id = ?");
    }

    @Override
    public void save(Session session) throws SQLException {
        insertStatement.setString(1, session.getUuid());
        insertStatement.setInt(2, session.getConnexion().getId());
        insertStatement.setString(3, session.getVm().getIp());
        insertStatement.execute();
    }

    @Override
    public Session getSession(String sessionId) throws SQLException, UnknownSessionException, UnknownConnexionException, UnknownVMException {
        findByIdStatement.setString(1, sessionId);
        ResultSet rs = findByIdStatement.executeQuery();
        if (rs.next()) {
            return new Session(sessionId,
                    connexionDAO.getById(rs.getInt(2)),
                    vmService.getVM(rs.getString(3)));
        } else {
            throw new UnknownSessionException(sessionId);
        }
    }

    @Override
    public void deleteById(String sessionUUID) throws SQLException, UnknownSessionException {
        deleteByIdStatement.setString(1, sessionUUID);
        int count = deleteByIdStatement.executeUpdate();
        if (count == 0) {
            throw new UnknownSessionException();
        }
    }

}
