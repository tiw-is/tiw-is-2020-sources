package fr.univlyon1.tiw1.gestionrdp.exception;

public class UnknownVMException extends Exception {
    public UnknownVMException() {
        super();
    }

    public UnknownVMException(String message) {
        super(message);
    }

    public UnknownVMException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownVMException(Throwable cause) {
        super(cause);
    }

    protected UnknownVMException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
