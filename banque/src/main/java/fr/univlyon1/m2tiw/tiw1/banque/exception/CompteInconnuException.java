package fr.univlyon1.m2tiw.tiw1.banque.exception;

public class CompteInconnuException extends Exception{
    public CompteInconnuException() {
    }

    public CompteInconnuException(String message) {
        super(message);
    }

    public CompteInconnuException(String message, Throwable cause) {
        super(message, cause);
    }

    public CompteInconnuException(Throwable cause) {
        super(cause);
    }

    public CompteInconnuException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
