package fr.univlyon1.m2tiw.tiw1.banque.compte;

import fr.univ_lyon1.tiw1_is.banque.service.ObjectFactory;
import fr.univ_lyon1.tiw1_is.banque.service.Transfert;
import fr.univ_lyon1.tiw1_is.banque.service.TransfertResponse;
import fr.univlyon1.m2tiw.tiw1.banque.exception.BanqueInconnueException;
import fr.univlyon1.m2tiw.tiw1.banque.exception.CompteInconnuException;
import fr.univlyon1.m2tiw.tiw1.banque.exception.SoldeInsuffisantException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class CompteEndpoint {
    public final static String NAMESPACE_URI = "http:/univ-lyon1.fr/tiw1-is/banque/service";
    private final ObjectFactory banqueObjectFactory = new ObjectFactory();

    @Autowired
    private CompteService compteService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "transfert")
    @ResponsePayload
    public TransfertResponse transfert(@RequestPayload Transfert transfert) {
        TransfertResponse rep = banqueObjectFactory.createTransfertResponse();
        try {
            compteService.virement(transfert.getSrcCompteId(),
                    transfert.getDestCompteId(),
                    transfert.getDestBanqueId(),
                    transfert.getAmount(),
                    transfert.getReference());
            rep.setOk(true);
        } catch (SoldeInsuffisantException | BanqueInconnueException | CompteInconnuException e) {
            rep.setOk(false);
            String name = e.getClass().getSimpleName();
            rep.setReason(name.substring(0, name.length() - "Exception".length()));
        }
        return rep;
    }
}
