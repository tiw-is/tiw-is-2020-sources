package fr.univlyon1.m2tiw.tiw1.banque.compte;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@RestController
@RequestMapping("/compte")
public class CompteController {

    @Autowired
    private CompteRepository compteRepository;

    @GetMapping
    public Collection<Compte> getAll() {
        return compteRepository.findAll();
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Compte> create(@RequestBody Compte compte) {
        Compte saved = compteRepository.save(compte);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Compte> getCompte(@PathVariable long id) {
        return compteRepository
                .findById(id)
                .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    @PatchMapping("/{id}")
    @Transactional
    public ResponseEntity<Compte> updateCompte(@PathVariable long id, @RequestBody Compte compte) {
        try {
            compte.setId(id);
            Compte updated = compteRepository.save(compte);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
