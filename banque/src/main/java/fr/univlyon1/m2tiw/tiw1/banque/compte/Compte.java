package fr.univlyon1.m2tiw.tiw1.banque.compte;

import fr.univlyon1.m2tiw.tiw1.banque.exception.SoldeInsuffisantException;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Compte {
    @Id
    private Long id;
    private double balance = 0.0;
    private double decouvertAutorise = 0.0;

    public Compte() {
    }

    public Compte(Long id, double balance, double decouvertAutorise) {
        this.id = id;
        this.balance = balance;
        this.decouvertAutorise = decouvertAutorise;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getDecouvertAutorise() {
        return decouvertAutorise;
    }

    public void setDecouvertAutorise(double decouvertAutorise) {
        this.decouvertAutorise = decouvertAutorise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compte compte = (Compte) o;
        return Objects.equals(id, compte.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void credit(double montant) {
        if (montant <= 0.0) {
            throw new IllegalArgumentException("Crédit négatif");
        }
        balance += montant;
    }

    public void debit(double montant) throws SoldeInsuffisantException {
        if (montant <= 0.0) {
            throw new IllegalArgumentException("Débit négatif");
        }
        if (balance + decouvertAutorise - montant <= 0) {
            throw new SoldeInsuffisantException("Solde insuffisant");
        }
        balance -= montant;
    }
}
