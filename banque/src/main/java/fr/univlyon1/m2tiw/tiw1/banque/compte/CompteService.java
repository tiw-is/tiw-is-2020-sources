package fr.univlyon1.m2tiw.tiw1.banque.compte;

import fr.univlyon1.m2tiw.tiw1.banque.exception.BanqueInconnueException;
import fr.univlyon1.m2tiw.tiw1.banque.exception.CompteInconnuException;
import fr.univlyon1.m2tiw.tiw1.banque.exception.SoldeInsuffisantException;
import fr.univlyon1.m2tiw.tiw1.banque.externe.BanqueExterneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
public class CompteService {
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private BanqueExterneService banqueExterneService;

    @Value("${tiw1.banque.id}")
    private String localBankId;

    @Transactional
    public void virementInterne(Compte source, Compte destination, double montant) throws SoldeInsuffisantException {
        source.debit(montant);
        destination.credit(montant);
    }

    @Transactional
    public void virement(Long sourceNumber,
                         Long destinationNumber,
                         String destinationBank,
                         double montant,
                         String reference)
            throws SoldeInsuffisantException, BanqueInconnueException, CompteInconnuException {
        try {
            if (localBankId.equals(destinationBank)) {
                Compte source = compteRepository.getOne(sourceNumber);
                Compte destination = compteRepository.getOne(destinationNumber);
                virementInterne(source, destination, montant);
            } else {
                compteRepository.getOne(sourceNumber).debit(montant);
                banqueExterneService.transfert(destinationBank, destinationNumber, montant, reference);
            }
        } catch (EntityNotFoundException e) {
            throw new CompteInconnuException(e);
        }
    }
}
