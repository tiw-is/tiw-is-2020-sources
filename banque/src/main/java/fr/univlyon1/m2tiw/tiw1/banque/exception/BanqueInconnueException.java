package fr.univlyon1.m2tiw.tiw1.banque.exception;

public class BanqueInconnueException extends Exception{
    public BanqueInconnueException() {
    }

    public BanqueInconnueException(String message) {
        super(message);
    }

    public BanqueInconnueException(String message, Throwable cause) {
        super(message, cause);
    }

    public BanqueInconnueException(Throwable cause) {
        super(cause);
    }

    public BanqueInconnueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
