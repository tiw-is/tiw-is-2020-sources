package fr.univlyon1.m2tiw.tiw1.banque.exception;

public class SoldeInsuffisantException extends Exception {
    public SoldeInsuffisantException() {
        super();
    }

    public SoldeInsuffisantException(String message) {
        super(message);
    }

    public SoldeInsuffisantException(String message, Throwable cause) {
        super(message, cause);
    }

    public SoldeInsuffisantException(Throwable cause) {
        super(cause);
    }

    protected SoldeInsuffisantException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
