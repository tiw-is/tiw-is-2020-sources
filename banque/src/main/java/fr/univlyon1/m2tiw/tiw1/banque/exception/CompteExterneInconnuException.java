package fr.univlyon1.m2tiw.tiw1.banque.exception;

public class CompteExterneInconnuException extends CompteInconnuException {
    public CompteExterneInconnuException() {
    }

    public CompteExterneInconnuException(String message) {
        super(message);
    }

    public CompteExterneInconnuException(String message, Throwable cause) {
        super(message, cause);
    }

    public CompteExterneInconnuException(Throwable cause) {
        super(cause);
    }

    public CompteExterneInconnuException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
