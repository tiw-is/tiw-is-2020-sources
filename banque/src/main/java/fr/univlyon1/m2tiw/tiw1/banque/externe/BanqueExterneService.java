package fr.univlyon1.m2tiw.tiw1.banque.externe;

import fr.univlyon1.m2tiw.tiw1.banque.exception.BanqueInconnueException;
import fr.univlyon1.m2tiw.tiw1.banque.exception.CompteExterneInconnuException;
import org.springframework.stereotype.Service;

@Service
public class BanqueExterneService {

    public String banqueEndpoint(String banqueId) throws BanqueInconnueException {
        // TODO: changer le code ci-dessous pour requêter le service banque-registry via Spring RestTemplate
        throw new BanqueInconnueException(banqueId);
    }

    public String banqueQueue(String banqueId) throws BanqueInconnueException {
        // TODO: changer le code ci-dessous pour requêter le service banque-registry via Spring RestTemplate
        throw new BanqueInconnueException(banqueId);
    }

    public boolean verifieCompte(String banqueId, Long compteId) throws BanqueInconnueException {
        String banqueExterneEndpoint = banqueEndpoint(banqueId);
        // TODO: changer ici pour faire une requête REST sur la banque externe afin de vérifier l'existence du compte.
        boolean compteExiste = false;
        return compteExiste;
    }

    public void transfert(String banqueId, Long compteId, double montant, String reference) throws CompteExterneInconnuException, BanqueInconnueException {
        if (verifieCompte(banqueId, compteId)) {
            // TODO: envoyer un message de transfert sur la bonne file rabbitmq
        } else {
            throw new CompteExterneInconnuException("banque: " + banqueId + ", compte: " + compteId + " inconnu");
        }
    }
}
