package fr.univlyon1.m2tiw.tiw1.registre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/banque/{nom}")
public class BanqueController {
    @Autowired
    private BanqueRepository banqueRepository;

    @GetMapping
    public ResponseEntity<Banque> getBanque(@PathVariable String nom) {
        var banqueOpt = banqueRepository.findById(nom);
        return banqueOpt
                .map(b -> new ResponseEntity<>(b, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping
    @Transactional
    public ResponseEntity<Banque> putBanque(@PathVariable String nom, @RequestBody Banque banque) {
        banque.setName(nom);
        Banque saved = banqueRepository.save(banque);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }
}
