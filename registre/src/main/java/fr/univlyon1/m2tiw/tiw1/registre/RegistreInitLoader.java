package fr.univlyon1.m2tiw.tiw1.registre;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

@Component
public class RegistreInitLoader implements CommandLineRunner {
    // See https://www.baeldung.com/running-setup-logic-on-startup-in-spring

    @Autowired
    private BanqueRepository banqueRepository;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        for (String filename : args) {
            List<Banque> banques = objectMapper.readValue(
                    new File(filename),
                    new TypeReference<List<Banque>>() {
                    });
            for (Banque b : banques) {
                if (!banqueRepository.existsById(b.getName())) {
                    banqueRepository.save(b);
                }
            }
        }
    }

}
