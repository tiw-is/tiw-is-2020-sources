package fr.univlyon1.m2tiw.tiw1.registre;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Banque {
    @Id
    private String name;
    private String transfertQueue;
    private String comptesBaseUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransfertQueue() {
        return transfertQueue;
    }

    public void setTransfertQueue(String transfertQueue) {
        this.transfertQueue = transfertQueue;
    }

    public String getComptesBaseUrl() {
        return comptesBaseUrl;
    }

    public void setComptesBaseUrl(String comptesBaseUrl) {
        this.comptesBaseUrl = comptesBaseUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Banque banque = (Banque) o;
        return Objects.equals(name, banque.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
