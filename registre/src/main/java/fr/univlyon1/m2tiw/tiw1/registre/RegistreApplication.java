package fr.univlyon1.m2tiw.tiw1.registre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistreApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistreApplication.class, args);
	}

}
