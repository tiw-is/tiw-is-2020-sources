#!/usr/bin/env bash

opts=""

if [[ -n "${JDBC_URL}" ]]
then
  opts="${opts} -Dspring.datasource.url=${JDBC_URL}"
fi
if [[ -n "${JDBC_DRIVER}" ]]
then
  opts="${opts} -Dspring.datasource.driverClassName=${JDBC_DRIVER}"
fi
if [[ -n "${JDBC_USERNAME}" ]]
then
  opts="${opts} -Dspring.datasource.username=${JDBC_USERNAME}"
fi
if [[ -n "${JDBC_PASSWORD}" ]]
then
  opts="${opts} -Dspring.datasource.password=${JDBC_PASSWORD}"
fi
if [[ -n "${JDBC_DIALECT}" ]]
then
  opts="${opts} -Dspring.datasource.database-platform=${JDBC_DIALECT}"
fi

cd /app
java ${opts} -jar registre*.jar "$@"
