package fr.univlyon1.m2tiw.tiw1;

import fr.univlyon1.m2tiw.tiw1.servers.JettyLauncher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Main démarré");

        JettyLauncher.run();
    }
}