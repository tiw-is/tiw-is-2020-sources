package fr.univlyon1.m2tiw.tiw1.gestionrdp;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

// Insipred from https://mkyong.com/spring-boot/spring-rest-spring-security-example/

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    // Create 2 users for demo
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                // {noop} required for proper password management
                // see e.g. https://www.baeldung.com/spring-security-5-default-password-encoder#2-nooppasswordencoder
                .withUser("user").password("{noop}usermdp").roles("USER")
                .and()
                .withUser("admin").password("{noop}adminmdp").roles("USER", "ADMIN");
    }

    // Secure the endpoins with HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/fournisseur/**").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/vm/**").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/vm/**/session").hasRole("USER")
                .antMatchers(HttpMethod.DELETE, "/vm/**/session/**").hasRole("USER")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }
}
