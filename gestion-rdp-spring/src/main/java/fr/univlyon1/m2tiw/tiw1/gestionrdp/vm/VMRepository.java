package fr.univlyon1.m2tiw.tiw1.gestionrdp.vm;

public interface VMRepository {
    VM getVM(String ip) throws UnknownVMException;
}
