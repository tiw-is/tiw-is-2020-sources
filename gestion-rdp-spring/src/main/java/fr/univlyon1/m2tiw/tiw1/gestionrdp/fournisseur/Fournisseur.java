package fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur;

import java.util.Objects;

public class Fournisseur {
    private String name;
    private int quota;
    private int consumed;

    public Fournisseur() {
    }

    public Fournisseur(String name, int quota, int consumed) {
        this.name = name;
        this.quota = quota;
        this.consumed = consumed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public int getConsumed() {
        return consumed;
    }

    public void setConsumed(int consumed) throws ConnexionQuotaExceededException {
        if (quota < consumed) {
            throw new ConnexionQuotaExceededException("Trying to use " + consumed + " connexions while quota is " + quota + " in Fournisseur " + name);
        }
        this.consumed = consumed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fournisseur that = (Fournisseur) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
