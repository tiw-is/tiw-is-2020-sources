package fr.univlyon1.m2tiw.tiw1.gestionrdp.session;

public class UnknownSessionException extends Exception {
    public UnknownSessionException() {
        super();
    }

    public UnknownSessionException(String message) {
        super(message);
    }

    public UnknownSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownSessionException(Throwable cause) {
        super(cause);
    }

    protected UnknownSessionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
