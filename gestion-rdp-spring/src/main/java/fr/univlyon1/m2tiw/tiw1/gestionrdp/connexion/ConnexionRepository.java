package fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnexionRepository extends JpaRepository<Connexion, Long> {
}
