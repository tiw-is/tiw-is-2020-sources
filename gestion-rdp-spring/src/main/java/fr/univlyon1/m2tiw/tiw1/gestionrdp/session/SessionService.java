package fr.univlyon1.m2tiw.tiw1.gestionrdp.session;

import fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion.Connexion;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion.ConnexionService;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion.UnknownConnexionException;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.ConnexionQuotaExceededException;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.vm.VM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class SessionService {
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private ConnexionService connexionService;

    @Transactional
    public Session createSession(VM vm) throws ConnexionQuotaExceededException {
        String id = UUID.randomUUID().toString();
        Connexion connexion = connexionService.openConnexion(vm.getFournisseur());
        Session session = new Session(id, connexion, vm);
        sessionRepository.save(session);
        return session;
    }

    @Transactional
    public void stopSession(String sessionId)
            throws UnknownSessionException, UnknownConnexionException {
        try {
            Session session = sessionRepository.getOne(sessionId);
            Connexion connexion = session.getConnexion();
            sessionRepository.deleteById(session.getUuid());
            connexionService.releaseConnexion(connexion.getId());
        } catch (EntityNotFoundException e) {
            throw new UnknownSessionException("Unknown session " + sessionId, e);
        }
    }
}
