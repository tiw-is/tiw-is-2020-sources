package fr.univlyon1.m2tiw.tiw1.gestionrdp.session;

import fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion.UnknownConnexionException;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.ConnexionQuotaExceededException;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.vm.UnknownVMException;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.vm.VM;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.vm.VMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vm/{ip}/session")
public class SessionController {

    @Autowired
    private VMRepository vmRepository;
    @Autowired
    private SessionService sessionService;

    @PostMapping
    public ResponseEntity<String> demarreSession(@PathVariable String ip) {
        ip = ip.replace("-", ".");
        try {
            VM vm = vmRepository.getVM(ip);
            Session session = sessionService.createSession(vm);
            return new ResponseEntity<>(session.getUuid(), HttpStatus.CREATED);
        } catch (UnknownVMException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ConnexionQuotaExceededException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping("/{sessionId}")
    public ResponseEntity<String> stopSession(@PathVariable String sessionId) {
        try {
            sessionService.stopSession(sessionId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (UnknownSessionException e) {
            return new ResponseEntity<>("Unknown session", HttpStatus.NOT_FOUND);
        } catch (UnknownConnexionException e) {
            return new ResponseEntity<>("Unknown connexion", HttpStatus.NOT_FOUND);
        }
    }
}
