package fr.univlyon1.m2tiw.tiw1.gestionrdp.session;


import fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion.Connexion;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.vm.VM;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.Objects;

@Entity
public class Session {
    @Id
    private String uuid;
    @ManyToOne
    private Connexion connexion;
    private String ip;
    @Transient
    private VM vm;

    public Session(String uuid, Connexion connexion, VM vm) {
        this.uuid = uuid;
        this.connexion = connexion;
        this.vm = vm;
    }

    public Session() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Connexion getConnexion() {
        return connexion;
    }

    public void setConnexion(Connexion connexion) {
        this.connexion = connexion;
    }

    public VM getVm() {
        return vm;
    }

    public void setVm(VM vm) {
        this.vm = vm;
        this.ip = vm.getIp();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return uuid.equals(session.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
