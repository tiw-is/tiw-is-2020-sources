package fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion;

import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.Fournisseur;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Objects;

@Entity
public class Connexion {
    @Id
    @GeneratedValue
    private Long id;
    private String fournisseurName;
    @Transient
    private Fournisseur fournisseur;

    public Connexion() {
    }

    public Connexion(Fournisseur fournisseur) {
        this.setFournisseur(fournisseur);
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
        this.fournisseurName = fournisseur.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFournisseurName() {
        return fournisseurName;
    }

    public void setFournisseurName(String fournisseurName) {
        this.fournisseurName = fournisseurName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Connexion connexion = (Connexion) o;
        return Objects.equals(id, connexion.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
