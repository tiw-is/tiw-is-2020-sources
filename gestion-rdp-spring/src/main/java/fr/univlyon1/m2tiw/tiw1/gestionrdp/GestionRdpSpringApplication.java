package fr.univlyon1.m2tiw.tiw1.gestionrdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionRdpSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionRdpSpringApplication.class, args);
    }

}
