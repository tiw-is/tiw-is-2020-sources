package fr.univlyon1.m2tiw.tiw1.gestionrdp.vm;

import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.FournisseurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VMRepositoryMock implements VMRepository {

    @Autowired
    private FournisseurRepository fournisseurRepository;

    @Override
    public VM getVM(String ip) throws UnknownVMException {
        if (!ip.endsWith("0")) {
            return new VM(ip, fournisseurRepository.getFournisseur("fournisseur1"));
        } else {
            throw new UnknownVMException(ip);
        }
    }
}
