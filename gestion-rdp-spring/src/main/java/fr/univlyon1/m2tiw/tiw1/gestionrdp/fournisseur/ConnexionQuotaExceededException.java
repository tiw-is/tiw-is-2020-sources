package fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur;

public class ConnexionQuotaExceededException extends Exception {
    public ConnexionQuotaExceededException() {
        super();
    }

    public ConnexionQuotaExceededException(String message) {
        super(message);
    }

    public ConnexionQuotaExceededException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnexionQuotaExceededException(Throwable cause) {
        super(cause);
    }

    protected ConnexionQuotaExceededException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
