package fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur;

import java.util.Collection;

public interface FournisseurRepository {
    Fournisseur getFournisseur(String name);

    Collection<Fournisseur> getAllFournisseurs();

    void updateFournisseur(Fournisseur f);
}
