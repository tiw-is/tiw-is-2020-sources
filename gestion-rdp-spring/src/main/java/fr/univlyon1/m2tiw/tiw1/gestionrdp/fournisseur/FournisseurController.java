package fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/fournisseur")
public class FournisseurController {

    @Autowired
    private FournisseurRepository fournisseurRepository;

    @GetMapping
    public ResponseEntity<Collection<Fournisseur>> fournisseurInfos() {
        return new ResponseEntity<>(fournisseurRepository.getAllFournisseurs(), HttpStatus.OK);
    }

}
