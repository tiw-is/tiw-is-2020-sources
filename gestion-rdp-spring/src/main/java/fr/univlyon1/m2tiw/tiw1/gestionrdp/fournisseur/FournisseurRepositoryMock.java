package fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FournisseurRepositoryMock implements FournisseurRepository {

    private Map<String, Fournisseur> fournisseurs = new HashMap<>();

    @Override
    public Fournisseur getFournisseur(String name) {
        return fournisseurs.computeIfAbsent(name, n -> new Fournisseur(n, 5, 0));
    }

    @Override
    public Collection<Fournisseur> getAllFournisseurs() {
        return Collections.unmodifiableCollection(fournisseurs.values());
    }

    @Override
    public void updateFournisseur(Fournisseur f) {
        // Nothing to do in mock
    }
}
