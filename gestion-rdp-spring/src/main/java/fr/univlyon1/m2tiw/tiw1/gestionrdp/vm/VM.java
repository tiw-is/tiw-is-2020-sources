package fr.univlyon1.m2tiw.tiw1.gestionrdp.vm;

import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.Fournisseur;

import java.util.Objects;

public class VM {
    private String ip;
    private Fournisseur fournisseur;

    public VM(String ip, Fournisseur fournisseur) {
        this.ip = ip;
        this.fournisseur = fournisseur;
    }

    public VM() {
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VM vm = (VM) o;
        return Objects.equals(ip, vm.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip);
    }
}
