package fr.univlyon1.m2tiw.tiw1.gestionrdp.connexion;

import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.ConnexionQuotaExceededException;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.Fournisseur;
import fr.univlyon1.m2tiw.tiw1.gestionrdp.fournisseur.FournisseurRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
@Slf4j
public class ConnexionService {

    @Autowired
    private ConnexionRepository connexionRepository;
    @Autowired
    private FournisseurRepository fournisseurRepository;

    @Transactional
    public Connexion openConnexion(Fournisseur fournisseur)
            throws ConnexionQuotaExceededException {
        if (fournisseur.getConsumed() >= fournisseur.getQuota()) {
            throw new ConnexionQuotaExceededException();
        }
        Connexion connexion = new Connexion();
        connexion.setFournisseur(fournisseur);
        connexionRepository.save(connexion);
        fournisseur.setConsumed(fournisseur.getConsumed() + 1);
        return connexion;
    }

    @Transactional
    public void releaseConnexion(Long connexionId) throws UnknownConnexionException {
        try {
            Connexion connexion = connexionRepository.getOne(connexionId);
            Fournisseur f = fournisseurRepository.getFournisseur(connexion.getFournisseurName());
            connexionRepository.deleteById(connexionId);
            f.setConsumed(f.getConsumed() - 1);
            fournisseurRepository.updateFournisseur(f);
        } catch (EntityNotFoundException e) {
            throw new UnknownConnexionException(e);
        } catch (ConnexionQuotaExceededException e) {
            log.error("Quota exceeded in while releasing connexion", e);
        }
    }

}
