package fr.univlyon1.m2tiw.tiw1.gestionrdp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest()
class GestionRdpSpringApplicationTests {

	@Test
	void contextLoads() {
		// Throws an exception when context won't load
		assertTrue(true);
	}

}
