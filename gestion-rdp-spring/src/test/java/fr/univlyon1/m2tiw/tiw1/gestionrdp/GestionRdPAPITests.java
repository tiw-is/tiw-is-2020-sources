package fr.univlyon1.m2tiw.tiw1.gestionrdp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GestionRdPAPITests {

    @Autowired
    private TestRestTemplate rt;

    @LocalServerPort
    private int port;

    private TestRestTemplate rta;

    @BeforeEach
    private void setup() {
        rta = rt.withBasicAuth("user", "usermdp");
    }

    @Test
    void testGetFournisseurs() {
        assertEquals("http://localhost:" + port, rt.getRootUri());
        var response = rta
                .getForEntity("/fournisseur", Collection.class);
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testGetFournisseurWithSessionActive() {
        String ip = "1-1-1-1";
        var r1 = rta.postForEntity("/vm/{ip}/session", "", String.class, ip);
        assertEquals(HttpStatus.CREATED, r1.getStatusCode());
        var r2 = rta.getForEntity("/fournisseur", Collection.class);
        assertEquals(HttpStatus.OK, r2.getStatusCode());
        assertNotNull(r2.getBody());
        assertTrue(r2.getBody().size() >= 1);
        rta.delete("/vm/{ip}/session/{sid}", ip, r1.getBody());
    }

    @Test
    void testCreateDeleteSession() {
        String ip = "1-1-1-1";
        var r1 = rta.postForEntity("/vm/{ip}/session", "", String.class, ip);
        assertEquals(HttpStatus.CREATED, r1.getStatusCode());
        String sid = r1.getBody();
        assertNotNull(sid);
        rta.delete("/vm/{ip}/session/{sid}", ip, sid);
    }
}
